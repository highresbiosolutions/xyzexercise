﻿using System;
using System.Collections.Generic;

namespace XyzExercise
{

    /* TODO:
     * 1. Implement System.MoveRobotToNest, which moves in straight lines to reach the target nest without crashing into plates.
     * 2. Create a verification mechanism to test.
     * 3. Add more test cases to cover the various edge cases.
     */

    class Program
    {
        static List<PlateType> _plateTypes = new List<PlateType>
        {
            new PlateType { Name = "Plate1", Height = 40 },
            new PlateType { Name = "Plate2", Height = 10 },
        };

        // These nests are lined up in a straight line at the same height.
        static List<Nest> _nests = new List<Nest>
        {
            new Nest { Name = "Nest1", Position = new Point { X = 10, Y = 10, Z = 50 } },
            new Nest { Name = "Nest2", Position = new Point { X = 50, Y = 10, Z = 50 } },
            new Nest { Name = "Nest3", Position = new Point { X = 90, Y = 10, Z = 50 } },
            new Nest { Name = "Nest4", Position = new Point { X = 130, Y = 10, Z = 50 } },
            new Nest { Name = "Nest5", Position = new Point { X = 170, Y = 10, Z = 50 } },
        };

        // The distance the robot needs to be above any nest or plate to travel over it.
        static double RobotClearance => 5;

        static void Main(string[] args)
        {
            RunTestCase1();
            // RunTestCase2();
            // RunTestCase3();

            Console.ReadLine();
        }

        static void RunTestCase1()
        {
            Console.WriteLine("Test Case 1");

            var currentInventory = new List<InventoryItem>
            {
                new InventoryItem { NestName = "Nest2", PlateName = "Plate1" },
                new InventoryItem { NestName = "Nest4", PlateName = "Plate2" }
            };

            // Currently over Nest1
            var robot = new Robot
            {
                CurrentPosition = new Point { X = 10, Y = 10, Z = 50 }
            };

            var system = new System(_nests, _plateTypes, currentInventory, robot);

            // Robot should move over to Nest5 without crashing into plates
            var pointsTraveled = system.MoveRobotToNest("Nest5");

            // TODO create a verification mechanism, for example:
            // Console.WriteLine(VerifyPoint(<todo>))
        }
    }

    class System
    {
        private List<Nest> _nests;
        private List<PlateType> _plateTypes;
        private List<InventoryItem> _inventoryItems;
        private Robot _robot;

        public System(List<Nest> nests, List<PlateType> plateTypes, List<InventoryItem> inventoryItems, Robot robot)
        {
            _nests = nests;
            _plateTypes = plateTypes;
            _inventoryItems = inventoryItems;
            _robot = robot;
        }

        // Return the points the robot moved to.
        public List<Point> MoveRobotToNest(string targetNestName)
        {
            var pointsTraveled = new List<Point>();

            // TODO

            return pointsTraveled;
        }
    }

    class PlateType
    {
        public string Name { get; set; }
        public double Height { get; set; }
    }

    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }

    class Nest
    {
        public string Name { get; set; }
        public Point Position { get; set; }
    }

    class Robot
    {
        public Point CurrentPosition { get; set; }

        public void MoveAbsoluteZ(double targetZ)
        {
            CurrentPosition.Z = targetZ;
        }

        public void MoveAbsoluteXY(double targetX, double targetY)
        {
            CurrentPosition.X = targetX;
            CurrentPosition.Y = targetY;
        }
    }

    class InventoryItem
    {
        public string NestName { get; set; }
        public string PlateName { get; set; }
    }
}
